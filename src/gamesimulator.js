const logger = require("./loggerfactory").createLogger();
const utils = require("./utils");

const initGame = function (url, payload, cb) {
    logger.info("posting game init request to server url=%s, payload=%s", url, payload);
    utils.invokeHTTPRequest(
        {
            url: url,
            body: JSON.stringify(payload)
        },
        (err, res) => {
            logger.info("game init server response err=%s,res=%s", err, res);
            if (err) return cb(err, null);
            return cb(null, res);
        }
    );
};
const Player = function (joinUrl) {
    this.joinUrl = joinUrl
}

const Simulator = {

    url: null,
    initialized: false,
    init: function (url, payload, cb) {
        this.url = null;
        let self = this;
        initGame(url, payload, (err, res) => {
            if (err) {
                self.url = null;
                cb(err, res);
            }
            this.initilized = true;
            cb(err, res);
        })
    }

}

module.exports = {Simulator}