const protobufjs = require("protobufjs");
const stringify = require("json-stringify");
const path = require("path");
const requireFromString = require("require-from-string");
const logger = require("./loggerfactory").createLogger();
const protoFiles = [
    "clientToserver.proto",
    "serverToClient.proto",
    "basemessage.proto"
];
const loaded = false;

var contracts = null;

const loadProtoFiles = function(cb) {
    if (loaded) {
        return cb(null, "success");
    }
    logger.info("Loading neutrino protobuff contract");
    let protoPackageBasePath = path.parse(require.resolve("neutrino-common")).dir;
    logger.info("protofiles basepath %s", protoPackageBasePath);
    let pbjs = require("protobufjs/cli/pbjs");
    let pbjsArgs = ["-t", "static-module"];
    protoFiles.forEach(f => {
        let protofile = path.join(protoPackageBasePath, "src", "proto", f);
        logger.info("Will load protofile %s", protofile);
        pbjsArgs.push(protofile);
    });
    logger.info("pbjs args %s", stringify(pbjsArgs));
    pbjs.main(pbjsArgs, (err, result) => {
        if (err) {
            logger.error({ err: err }, "Error in loading protobuf contract");
            return cb(err);
        }
        contracts = result;
        logger.trace("Generated JS file %s", contracts);
        cb();
    });
};

const get = function(){return contracts};

module.exports = {loadProtoFiles,get};