const http = require("http");

const invokeHTTPRequest = function (options, cb) {

    let req = http.request(options, res => {
        let buffer = "";
        res.on("data", data => {
            buffer += data;
        });
        res.once("end", () => {
            cb(null, {statusCode: res.statusCode, data: buffer});
        });
    });
    req.on("error", err => {
        cb(err, null);
    });
    if (options.body) {
        req.write(options.body);
    }
    req.end();
};

module.exports={invokeHTTPRequest};