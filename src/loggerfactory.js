const bunyan = require("bunyan");

let logger = null;
const createLogger = function() {
    if (logger) return logger;
    let level = process.env.LOGLEVEL || "trace";
    logger = bunyan.createLogger({
        name: "neutrino",
        stream: process.out,
        level: level,
        src: true
    });
    return logger;
};

module.exports = {createLogger}