const {ipcMain} = require('electron')
const gameSimulator = require("../gamesimulator").Simulator;

const submitGameInitRequest = function (url, payload, cb) {

    gameSimulator.init(url, payload, cb);
}

const bindHandlers = function () {
    ipcMain.handle("game-init", (event, args) => {
        return retPm = new Promise((resolve, reject) => {
            submitGameInitRequest(args[0], args[1], (err, resp) => {
                if (err) {
                    resolve(err);
                }
                resolve(resp);
            });
        })

    });

}

module.exports = {bindHandlers};