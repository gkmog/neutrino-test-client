const {ipcRenderer} = require('electron');
const logger = require("../loggerfactory").createLogger();

const gameiniturlSubmit = function () {
    logger.info("Game init pressed");
    let gameInitTextConst = document.getElementById("_game_init_url_txt");
    let url = gameInitTextConst.value;
    let gameInitPayLoadText = document.getElementById("_game_init_payload_txt");
    let payload = gameInitPayLoadText.value;
    let gameInitResTextArea = document.getElementById("_game_init_res_txt");
    ipcRenderer.invoke("game-init", [url, payload]).then((val) => {
        gameInitResTextArea.value = val
    });
    logger.info("submitted game init request to main with values url = %s, payload = %s", url, payload);
}


//direct execution on page
const bindListeners = function () {
    let gameInitSubmitButton = document.getElementById("_game_init_submit_btn");
    gameInitSubmitButton.addEventListener("click", gameiniturlSubmit);
}
alert("hi");
bindListeners();
